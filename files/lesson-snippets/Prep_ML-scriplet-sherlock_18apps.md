<!--

Taken out 2023-08-14 from 24-keras-classify.md.
This snippet was the second version of data preparation for sherlock_18apps
dataset.

The approach below uses a prepared "scriplet" that can be "sourced"
(or `%load`-ed) into the current Python environment in order to
reliably execute a prescribed set of Python commands.
It can be useful in a limited number of circumstances, particularly for ad-hoc analyses.
Ad-hoc things are probably quite common for interactive Jupyter sessions
and convenient to use,
since sourced scriplets appear simpler and more straightforward than
splicing things into functions.
For example, one does not have to think harder to organize
where certain bits of data should be placed,
or what group of statements logically constitutes a function. 
However, the `%load` does not work for standard Python scripting
(although `exec(open(FILENAME, 'r').read()` can work similarly).
A major issue with sourced scriplets is that all variables would be
created and stored in the global namespace.
This may be what we want---or may not be!
Such an arrangement is convenient for ad-hoc work,
but can quickly lead to a disorganized computing environment when
the analysis scales up and becomes more complex. 
For this reason, we favor the use of standard Python functions and modules
so that the analysis work can translate better to
a reproducible and fully-automated pipeline using non-interactive Python scripting.

-->


The script `Prep_ML.py` contains all the steps necessary to read
the data, remove useless data, handle missing data, extract the feature matrix 
and labels, then do the train/dev split. 
Load the commands contained in this script into your current Jupyter notebook
using the IPython's `%load` magic command:
~~~python
%load Prep_ML.py
~~~

Press <kbd>Shift</kbd>+<kbd>Enter</kbd> to execute,
then the contents of the script will be loaded into the active cell.
The cell would "magically" change to:


~~~python
"""Data-preparation-only scriplet.
"""

import sys

import pandas as pd
import numpy as np
import sklearn

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix

df = pd.read_csv("sherlock/sherlock_18apps.csv")

## Summarize the dataset
print("* shape:", df.shape)
print()
print("* info::\n")
df.info()
print()
print("* describe::\n")
print(df.describe().T)
print()

"""Do standard preprocessing of a sherlock_18apps dataset.
All the obviously bad and missing data are removed.
"""
# Delete irrelevant feature(s)
del_features_irrelevant = [
    'Unnamed: 0'  # mere integer index
]

# Missing data or bad data
del_features_bad = [
    'cminflt', # all-missing feature
    'guest_time', # all-flat feature
]
del_features = del_features_irrelevant + del_features_bad

df2 = df.drop(del_features, axis=1)

print("Preprocessing:")
print("- dropped %d columns: %s" % (len(del_features), del_features))
print("- remaining missing data:")

isna_counts = df2.isna().sum()

print(isna_counts[isna_counts > 0])
print("- dropping the rest of missing data")

df2.dropna(inplace=True)

print("- remaining shape: %s" % (df2.shape,))
sys.stdout.flush()
print()

## Separate labels from the features
labels = df2['ApplicationName']
df_features = df2.drop('ApplicationName', axis=1)

"""Perform one-hot encoding for **all** categorical features."""
print("Step: Converting all non-numerical features to one-hot encoding.")
df_features = pd.get_dummies(df_features)

"""Step: Feature scaling using StandardScaler."""
print("Step: Feature scaling with StandardScaler")

df = df_features
df_features_unscaled = df
scaler = preprocessing.StandardScaler()
scaler.fit(df)

# Recast the features still in a dataframe form
df_features = pd.DataFrame(scaler.transform(df),
                           columns=df.columns,
                           index=df.index)
print("After scaling:")
print(df_features.head(10))
print()

"""Step: Perform train-test split on the master dataset.
This should be the last step before constructing & training the model.
"""
test_size = 0.2
random_state = 34

print("Step: Train-test split  test_size=%s  random_state=%s" \
      % (test_size, random_state))

train_features, test_features, train_labels, test_labels = \
                        train_test_split(df_features, labels,
                        test_size=test_size, random_state=random_state)

tt_split_params = dict(test_size=test_size, random_state=random_state)

print("- training dataset: %d records" % (len(train_features),))
print("- testing dataset:  %d records" % (len(test_features),))
sys.stdout.flush()

print("Now the dataset is ready for machine learning!")
~~~

> ## What's in the Script?
> <!--- For "NN on HPC" episode-->
> <!--- (Jupyter Notebook is a convenient platform for performing interactive development
> in Python programming language.
> However, when a project progresses, the programs will become more complex,
> and frequently we will find parts and pieces that are used over and over.
> We recommend that complex steps that are used over and over
> are broken down into *functions* and saved into Python scripts
> (text files with the `.py` extension). -->
>
> In the first step above we are using a single Python script: `Prep_ML.py`.
> The contents of first script are loaded into a cell in our notebook
> using the `%load` IPython magic.
> This approach works well when the content of the script is not too long.
> File `Prep_ML.py` contains many lines, but there will be a new approach 
> introduced in a future lesson to improve this.
> 
{: .callout}


> ## Executing the Loaded Program
>
> Press <kbd>Shift</kbd>+<kbd>Enter</kbd> the second time to execute
> the Python statements `%load`ed in the cell now.
>
> > ## Solution
> > ~~~Output
> > * shape: (273129, 20)
> >
> > * info::
> >
> > <class 'pandas.core.frame.DataFrame'>
> > RangeIndex: 273129 entries, 0 to 273128
> > Data columns (total 20 columns):
> > Unnamed: 0           273129 non-null int64
> > ApplicationName      273129 non-null object
> > CPU_USAGE            273077 non-null float64
> > UidRxBytes           273129 non-null int64
> > UidRxPackets         273129 non-null int64
> > UidTxBytes           273129 non-null int64
> > UidTxPackets         273129 non-null int64
> > cutime               273077 non-null float64
> > guest_time           273077 non-null float64
> > importance           273129 non-null int64
> > lru                  273129 non-null int64
> > num_threads          273077 non-null float64
> > otherPrivateDirty    273129 non-null int64
> > priority             273077 non-null float64
> > rss                  273077 non-null float64
> > state                273077 non-null object
> > stime                273077 non-null float64
> > utime                273077 non-null float64
> > vsize                273077 non-null float64
> > cminflt              0 non-null float64
> > dtypes: float64(10), int64(8), object(2)
> > memory usage: 41.7+ MB
> >
> > * describe::
> >
> >                       count          mean           std    min           25%  \
> > Unnamed: 0         273129.0  5.281532e+05  2.876889e+05    0.0  2.806510e+05   
> > CPU_USAGE          273077.0  6.618322e-01  3.207833e+00    0.0  5.000000e-02   
> > UidRxBytes         273129.0  3.922973e+02  3.693198e+04 -280.0  0.000000e+00   
> > UidRxPackets       273129.0  4.204643e-01  2.790607e+01  -11.0  0.000000e+00   
> > UidTxBytes         273129.0  2.454729e+02  2.977305e+04  -60.0  0.000000e+00   
> > UidTxPackets       273129.0  3.878826e-01  2.420920e+01   -1.0  0.000000e+00   
> > cutime             273077.0  3.279844e-01  1.768488e+00    0.0  0.000000e+00   
> > guest_time         273077.0  0.000000e+00  0.000000e+00    0.0  0.000000e+00   
> > importance         273129.0  3.139921e+02  8.891191e+01  100.0  3.000000e+02   
> > lru                273129.0  4.712480e+00  6.348188e+00    0.0  0.000000e+00   
> > num_threads        273077.0  3.928061e+01  2.682408e+01    2.0  1.700000e+01   
> > otherPrivateDirty  273129.0  1.211232e+04  2.026702e+04    0.0  1.480000e+03   
> > priority           273077.0  1.975093e+01  1.170649e+00    9.0  2.000000e+01   
> > rss                273077.0  8.500590e+03  4.942350e+03    0.0  4.894000e+03   
> > stime              273077.0  1.378527e+03  3.568420e+03    3.0  9.100000e+01   
> > utime              273077.0  2.509427e+03  5.325113e+03    2.0  1.020000e+02   
> > vsize              273077.0  2.049264e+09  1.179834e+08    0.0  1.958326e+09   
> > cminflt                 0.0           NaN           NaN    NaN           NaN   
> >
> >                             50%           75%           max  
> > Unnamed: 0         5.519380e+05  7.839780e+05  9.999940e+05  
> > CPU_USAGE          1.300000e-01  3.700000e-01  1.108900e+02  
> > UidRxBytes         0.000000e+00  0.000000e+00  8.872786e+06  
> > UidRxPackets       0.000000e+00  0.000000e+00  6.165000e+03  
> > UidTxBytes         0.000000e+00  0.000000e+00  9.830372e+06  
> > UidTxPackets       0.000000e+00  0.000000e+00  6.748000e+03  
> > cutime             0.000000e+00  0.000000e+00  1.100000e+01  
> > guest_time         0.000000e+00  0.000000e+00  0.000000e+00  
> > importance         3.000000e+02  4.000000e+02  4.000000e+02  
> > lru                0.000000e+00  1.100000e+01  1.600000e+01  
> > num_threads        3.000000e+01  5.500000e+01  1.410000e+02  
> > otherPrivateDirty  4.308000e+03  1.354800e+04  1.928560e+05  
> > priority           2.000000e+01  2.000000e+01  2.000000e+01  
> > rss                6.959000e+03  1.120600e+04  5.466800e+04  
> > stime              3.450000e+02  1.474000e+03  4.662900e+04  
> > utime              5.650000e+02  2.636000e+03  4.284500e+04  
> > vsize              2.026893e+09  2.125877e+09  2.456613e+09  
> > cminflt                     NaN           NaN           NaN  
> >
> > Preprocessing:
> > - dropped 3 columns: ['Unnamed: 0', 'cminflt', 'guest_time']
> > - remaining missing data:
> > CPU_USAGE      52
> > cutime         52
> > num_threads    52
> > priority       52
> > rss            52
> > state          52
> > stime          52
> > utime          52
> > vsize          52
> > dtype: int64
> > - dropping the rest of missing data
> > - remaining shape: (273077, 17)
> >
> > Step: Converting all non-numerical features to one-hot encoding.
> > Non-numeric features:
> > - O  ApplicationName
> > - O  state
> >
> > Step: Feature scaling with StandardScaler
> > After scaling:
> >    CPU_USAGE  UidRxBytes  UidRxPackets  UidTxBytes  UidTxPackets    cutime  \
> > 0  -0.165792   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 1   0.308049   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 2  -0.140853   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 3  -0.196966   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 4  -0.143970   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 5  -0.047332   -0.003421      0.092426   -0.001260      0.149189 -0.185461   
> > 6  -0.196966   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 7  -0.200083   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 8  -0.181379   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> > 9  -0.206318   -0.010623     -0.015068   -0.008245     -0.016022 -0.185461   
> >
> >    importance       lru  num_threads  otherPrivateDirty  priority       rss  \
> > 0    0.967513  1.621094    -0.271421          -0.188207  0.212762  0.497013   
> > 1    0.967513  1.621094    -0.830621           0.748432  0.212762  2.335413   
> > 2   -0.157189 -0.742150     1.219779          -0.039008  0.212762  1.090659   
> > 3   -0.157189 -0.742150    -0.942461          -0.555284  0.212762 -0.567867   
> > 4   -0.157189 -0.742150     1.406179          -0.312737  0.212762  0.002106   
> > 5   -0.157189 -0.742150     2.039939           0.680739  0.212762  1.858918   
> > 6    0.967513  1.621094    -0.532381          -0.336024  0.212762 -0.112617   
> > 7   -0.157189 -0.742150    -1.054301          -0.593373  0.212762 -0.918915   
> > 8    0.967513  1.305995    -0.718781          -0.435688  0.212762 -0.369580   
> > 9   -1.281891 -0.742150    -1.091581          -0.570282  0.212762 -1.120843   
> >
> >       stime     utime     vsize   state_D   state_R   state_S  state_Z  
> > 0 -0.335871 -0.412842  0.127145 -0.020436 -0.060473  0.064346 -0.00789  
> > 1 -0.367538 -0.431809 -0.010889 -0.020436 -0.060473  0.064346 -0.00789  
> > 2 -0.275620 -0.369463  0.487610 -0.020436 -0.060473  0.064346 -0.00789  
> > 3 -0.374264 -0.463921 -1.320928 -0.020436 -0.060473  0.064346 -0.00789  
> > 4 -0.273939 -0.384110  1.316752 -0.020436 -0.060473  0.064346 -0.00789  
> > 5 -0.108319 -0.140735  1.898536 -0.020436 -0.060473  0.064346 -0.00789  
> > 6 -0.379588 -0.463921 -1.018926 -0.020436 -0.060473  0.064346 -0.00789  
> > 7 -0.381830 -0.461104 -1.064197 -0.020436 -0.060473  0.064346 -0.00789  
> > 8 -0.352405 -0.439696 -0.751398 -0.020436 -0.060473  0.064346 -0.00789  
> > 9 -0.343438 -0.459038 -1.089401 -0.020436 -0.060473  0.064346 -0.00789  
> >
> > Step: Train-test split  test_size=0.2  random_state=34
> > - training dataset: 218461 records
> > - testing dataset:  54616 records
> > Now the dataset is ready for machine learning!
> > ~~~
> > {: .output}
> {: .solution}
{: .challenge}


> ## Verbose Output
>
> The script provides rather verbose output,
> as we plan to run this non-interactively later on.
> Unlike Jupyter notebook cells, where the value of the last statement in a cell
> is printed as the output value of that cell,
> Python scripts require values to be explicitly `print`ed.
> The verbose output is essential for several reasons:
>
> 1. To provide a record of the calculation for the sake of *reproducibility*.
>    For example, printing the `head`, `tail`, and/or `describe` of
>    a dataframe object would provide a short record of your input data.
>    Another good idea is to print the name of the input file.
>    This can be important when you have so many datasets to process.
>
>    In research, reproducibility is a very important thing to have.
>    We must be able to track the origin of every result and conclusion of our analysis.
>    For this reason, we need to keep the record of the *input data* and
>    the *process* by which we arrive at our results.
>    The use of scripts and digital notebooks
>    (such as Python and Jupyter notebooks) can greatly facilitate this.
>
> 2. We need to verify the correctness of the script.
>    Printing key intermediate values can provide useful "check points"
>    before we arrive at the final results.
>
> 3. When troubles come (script crashes or errors), printing helps pinpoint
>    the source of the troubles (for example in our case,
>    an average value of `CPU_USAGE` that is negative).
>
> **How verbose does one ought to print?**
> This depends on your sense of judgment as the script writer.
> For development purposes, you will want to ensure that at every step,
> your script is doing the right thing.
> However, for deployment, once the script is validated with test cases,
> you may not want to print as many values, as this deluge of printouts would
> easily cause us to miss the important signals.
> Many scripts and programs have a way to control the level of verbosity---with
> verbose output enabled during development/debugging, and minimal printout during
> production calculations.
{: .callout}
