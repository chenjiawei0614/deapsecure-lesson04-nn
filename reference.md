---
layout: reference
---

## Glossary

{:auto_ids}
variance
:   A [path](#path) that refers to a particular location in a file system.
    Absolute paths are usually written with respect to the file system's
FIXME


## Further Reading

### Building neural networks

***"Pipelines, Mind Maps and Convolutional Neural Networks"*** 
<https://towardsdatascience.com/pipelines-mind-maps-and-convolutional-neural-networks-34bfc94db10c>

This article describes the discipline that a data scientist exerted
over himself and his impulses so that he could get to his end-goal
more efficiently.

He was using a "mind map" to help keep track what one has done in
changing network, etc. (as well as the effect of each change)
to achieve a better-performing network.

{% include links.md %}
