---
title: "Using Multicore CPUs or GPUs for KERAS Computation"
teaching: 0
exercises: 0
questions:
- "How do we use multicore CPUs for KERAS computations?"
- "How do we use GPUs for KERAS computations?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

The neural network training process takes a long time.
We can speed this up by using multiple CPU cores, or better yet,
GPUs.

## Running on Multicore CPUs

As an example, let's run the previous training on 8 CPU cores.
The `salloc` statement needs to become:

    $ salloc -c 8 -C AVX2

then modify the part in `url_analyzer.py` so that
the `intra_op_parallelism_threads` and `inter_op_parallelism_threads`
parameters are given value of 8.
Then run the training process again.

> ## Measuring parallel speed up
> How many times faster did the calculation go compared to the single-core case?
{:.challenge}

## Running on GPU

To run the training on a GPU, please issue the following resource request
instead:

    $ salloc -p timed-gpu --gres=gpu:1

IMPORTANT: We have only a few GPUs on Turing, not enough for everyone to do this
experiment.
It is best if you submit a batch job to maximize the utilization of GPU.

## Running batch job

A sample job script is provided in your hands-on directory, `url_job.sh`.
To run this job, you need to create a Python script named `url_analyzer_script.py`
that includes the commands for training & cross-validating the model.
Start by creating a copy of `url_analyzer.py`.

{% include links.md %}

