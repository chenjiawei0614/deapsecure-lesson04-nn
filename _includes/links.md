[cc-by-human]: https://creativecommons.org/licenses/by/4.0/
[cc-by-legal]: https://creativecommons.org/licenses/by/4.0/legalcode
[ci]: http://communityin.org/
[coc-reporting]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#reporting-guidelines
[coc]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html
[concept-maps]: https://carpentries.github.io/instructor-training/05-memory/
[contrib-covenant]: https://contributor-covenant.org/
[contributing]: {{ repo_url }}/blob/{{ source_branch }}/CONTRIBUTING.md
[cran-checkpoint]: https://cran.r-project.org/package=checkpoint
[cran-knitr]: https://cran.r-project.org/package=knitr
[cran-stringr]: https://cran.r-project.org/package=stringr
[dc-lessons]: http://www.datacarpentry.org/lessons/
[email]: mailto:team@carpentries.org
[github-importer]: https://import.github.com/
[importer]: https://github.com/new/import
[jekyll-collection]: https://jekyllrb.com/docs/collections/
[jekyll-install]: https://jekyllrb.com/docs/installation/
[jekyll-windows]: http://jekyll-windows.juthilo.com/
[jekyll]: https://jekyllrb.com/
[jupyter]: https://jupyter.org/
[lc-lessons]: https://librarycarpentry.org/#portfolio
[lesson-example]: https://carpentries.github.io/lesson-example/
[mit-license]: https://opensource.org/licenses/mit-license.html
[morea]: https://morea-framework.github.io/
[numfocus]: https://numfocus.org/
[osi]: https://opensource.org
[pandoc]: https://pandoc.org/
[paper-now]: https://github.com/PeerJ/paper-now
[python-gapminder]: https://swcarpentry.github.io/python-novice-gapminder/
[pyyaml]: https://pypi.python.org/pypi/PyYAML
[r-markdown]: https://rmarkdown.rstudio.com/
[rstudio]: https://www.rstudio.com/
[ruby-install-guide]: https://www.ruby-lang.org/en/downloads/
[ruby-installer]: https://rubyinstaller.org/
[rubygems]: https://rubygems.org/pages/download/
[styles]: https://github.com/carpentries/styles/
[swc-lessons]: https://software-carpentry.org/lessons/
[swc-releases]: https://github.com/swcarpentry/swc-releases
[workshop-repo]: {{ site.workshop_repo }}
[yaml]: http://yaml.org/

[ipython]: https://ipython.org/
[tensorflow]: https://www.tensorflow.org/
[keras]: https://keras.io/
[cntk]: https://docs.microsoft.com/en-us/cognitive-toolkit/
[theano]: http://deeplearning.net/software/theano/

{% comment %} DeapSECURE-specific links {% endcomment %}

[deapsecure-website]: https://deapsecure.gitlab.io/
[deapsecure-hpc-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/
[deapsecure-bd-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson02-bd/
[deapsecure-ml-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson03-ml/
[deapsecure-nn-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson04-nn/
[deapsecure-crypt-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/
[deapsecure-par-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson06-par/

[deapsecure-bd-sherlock-overview]: https://deapsecure.gitlab.io/deapsecure-lesson02-bd/02-big-data-sherlock/index.html
[deapsecure-ml-preprocessing]: https://deapsecure.gitlab.io/deapsecure-lesson03-ml/20-preprocessing/index.html
[deapsecure-python-courses]: https://deapsecure.gitlab.io/posts/2020/01/crash-course-python/

[sherlock-overview]: http://bigdata.ise.bgu.ac.il/sherlock/#/
[sherlock-pub-2016]: https://dl.acm.org/doi/10.1145/2996758.2996764
{% comment %} This is now invalid as of 2023 -- Need to re-host on our drive {% endcomment %}
[sherlock-desc-PDF]: https://drive.google.com/file/d/0B_A1qX1kf7R9Q0llRWpkY2pXdzg/view

{% comment %} Portions of the lesson in *this* module {% endcomment %}

[NN-ep20-keras-intro]: {{page.root}}{% link _episodes/20-keras-intro.md %}
[NN-ep24-keras-classify]: {{page.root}}{% link _episodes/24-keras-classify.md %}
[NN-ep24-keras-classify_preprocess]: {{page.root}}{% link _episodes/24-keras-classify.md %}#data-preprocess
[sherlock-2apps-preprocessed]: {{page.root}}{% link _episodes/02-sherlock-apps-ml.md %}#sherlock_2apps

{% comment %} Portions of the lesson in *other* DeapSECURE modules {% endcomment %}

[BD-ep30-data-wrangling-viz_data-types]: {{site.deapsecure_bd_lesson}}/30-data-wrangling-viz/index.html#types-of-data

{% comment %} External learning resources {% endcomment %}

[MOOC-scikit-learn]: https://inria.github.io/scikit-learn-mooc/
[MOOC-scikit-learn_categorical]: https://inria.github.io/scikit-learn-mooc/python_scripts/03_categorical_pipeline.html

[intro-weight-init-NN]: https://wandb.ai/sauravmaheshkar/initialization/reports/A-Gentle-Introduction-To-Weight-Initialization-for-Neural-Networks--Vmlldzo2ODExMTg?galleryTag=beginner
